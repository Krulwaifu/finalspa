package kz.aitu.finalspa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalspaApplication {

    public static void main(String[] args) {
        SpringApplication.run(FinalspaApplication.class, args);
    }

}
