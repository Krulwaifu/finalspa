package kz.aitu.finalspa.service;

import kz.aitu.finalspa.model.Auth;
import kz.aitu.finalspa.model.User;
import kz.aitu.finalspa.repository.AuthRepository;
import kz.aitu.finalspa.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private final AuthRepository authRepository;
    private final UserRepository userRepository;

    public String login(String login, String password) {
        Auth auth = authRepository.getByLoginAndPassword(login,password);
        if (auth != null){
        UUID uuid = UUID.randomUUID();
        String uuidAsString = uuid.toString();
        auth.setToken(uuidAsString);
        authRepository.save(auth);
        return auth.getToken();
        }
        return "no such user";
    }
    public User register(String name, String login, String password) {
        User user = userRepository.getUserByName(name);
        if (user != null){
            Auth auth = new Auth();
            auth.setLogin(login);
            auth.setPassword(password);
            auth.setUserId(user.getId());
            authRepository.save(auth);
            return user;
        }
        return null;
    }
    public Boolean isExistByToken(String token) {
        return authRepository.existsByToken(token);
    }

    public List<Auth> getAll() {
        return authRepository.findAll();
    }

    public void update(Auth auth) {
        authRepository.save(auth);
    }

    public void delete(Auth auth) {
        authRepository.delete(auth);
    }

    public User getUserByToken(String token) {
        Auth auth = authRepository.getByToken(token);
        return userRepository.getUserById(auth.getUserId());
    }

    public void add(Auth auth) {
        authRepository.save(auth);
    }

    public User checkLogin(String token) {
        Auth auth = authRepository.getByToken(token);
        if (auth != null){
            return userRepository.getUserById(auth.getUserId());
        }
        return null;
    }
}
