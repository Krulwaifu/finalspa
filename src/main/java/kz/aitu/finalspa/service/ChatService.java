package kz.aitu.finalspa.service;

import kz.aitu.finalspa.model.Chat;
import kz.aitu.finalspa.repository.ChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ChatService {
    private final ChatRepository chatRepository;

    public void add(Chat chat) {
        chatRepository.save(chat);
    }

    public List<Chat> getAll() {
        return chatRepository.findAll();
    }

    public void update(Chat chat) {
        chatRepository.save(chat);
    }

    public void delete(Chat chat) {
        chatRepository.delete(chat);
    }
}
