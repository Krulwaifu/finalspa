package kz.aitu.finalspa.service;

import kz.aitu.finalspa.model.Message;
import kz.aitu.finalspa.repository.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;

    public List<Message> getMessagesByChat(Long chatId) {
        return messageRepository.findMessagesByChatId(chatId);
    }

    public void add(Message message) {
        messageRepository.save(message);
    }

    public void update(Message message) {
        messageRepository.save(message);
    }

    public void delete(Message message) {
        messageRepository.delete(message);
    }

    public boolean isExistByChatAndUser(Long chatid,Long userid){
        return messageRepository.existsByChatIdAndUserId(chatid, userid);
    }
}
