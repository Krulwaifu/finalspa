package kz.aitu.finalspa.controller;

import kz.aitu.finalspa.model.Chat;
import kz.aitu.finalspa.service.AuthService;
import kz.aitu.finalspa.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/chat")
public class ChatController {
    private final ChatService chatService;
    private final AuthService authService;

    @GetMapping
    public ResponseEntity<?> getAll(@RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            return ResponseEntity.ok(chatService.getAll());
        }
        return ResponseEntity.ok("Error:Please login");
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Chat chat,
                                 @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            chatService.add(chat);
            return ResponseEntity.ok("Chat successfully added");
        }
        return ResponseEntity.ok("Error:Please login");
    }

    @PutMapping
    public ResponseEntity<?> edit(@RequestBody Chat chat,
                                  @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            chatService.update(chat);
            return ResponseEntity.ok(chat);
        }
        return ResponseEntity.ok("Error:Please login");
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Chat chat,
                                    @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            chatService.delete(chat);
            return ResponseEntity.ok("Chat successfully deleted");
        }
        return ResponseEntity.ok("Error:Please login");
    }
}
