package kz.aitu.finalspa.controller;


import kz.aitu.finalspa.model.Auth;
import kz.aitu.finalspa.model.User;
import kz.aitu.finalspa.service.AuthService;
import kz.aitu.finalspa.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {
    private final UserService userService;
    private final AuthService authService;

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody User user) {
        userService.add(user);
        return ResponseEntity.ok("User successfully added");
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody User user) {
        userService.update(user);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody User user, @RequestBody Auth auth) {
        authService.delete(auth);
        userService.delete(user);
        return ResponseEntity.ok("User successfully deleted");
    }
}
