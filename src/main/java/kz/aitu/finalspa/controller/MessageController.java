package kz.aitu.finalspa.controller;

import kz.aitu.finalspa.model.Message;
import kz.aitu.finalspa.model.User;
import kz.aitu.finalspa.service.AuthService;
import kz.aitu.finalspa.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/message")
public class MessageController {
    private final MessageService messageService;
    private final AuthService authService;

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Message message,
                                 @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            messageService.add(message);
            return ResponseEntity.ok("Message successfully added");
        }
        return ResponseEntity.ok("Error:Please login");
    }

    @PutMapping
    public ResponseEntity<?> edit(@RequestBody Message message,
                                  @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            messageService.update(message);
            return ResponseEntity.ok(message);
        }
        return ResponseEntity.ok("Error:Please login");
    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Message message,
                                    @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            messageService.delete(message);
            return ResponseEntity.ok("Message successfully deleted");
        }
        return ResponseEntity.ok("Error:Please login");
    }

    @GetMapping("/messages/{chatId}")
    public ResponseEntity<?> getMessagesByChatId(@PathVariable Long chatId,
                                                 @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            User user = authService.getUserByToken(token);
            if (messageService.isExistByChatAndUser(chatId, user.getId())) {
                return ResponseEntity.ok(messageService.getMessagesByChat(chatId));
            } else {
                return ResponseEntity.ok("Error: This user is not in this chat");
            }
        }
        return ResponseEntity.ok("Error:Please login");
    }
}
