package kz.aitu.finalspa.repository;

import kz.aitu.finalspa.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message,Long> {
    List<Message> findMessagesByChatId(Long chatId);
    boolean existsByChatIdAndUserId(Long chatid, Long userid);
}
