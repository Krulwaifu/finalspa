package kz.aitu.finalspa.repository;

import kz.aitu.finalspa.model.Auth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthRepository extends JpaRepository<Auth, Long> {

    Auth getByToken(String token);
    Boolean existsByToken(String token);
    Auth getByLoginAndPassword(String login, String password);
}
